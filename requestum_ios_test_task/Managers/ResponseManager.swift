//
//  ResponseManager.swift
//  requestum_ios_test_task
//
//  Created by Dmitry Anatolievich on 01.04.2021.
//

import Foundation
import RxSwift
import RxCocoa

protocol ResponseProtocol: Decodable {
    var errors: ErrorType? { get }
}

class ResponseManager: NSObject {
    func parsingData<T: Decodable, U>(observer: AnyObserver<U>, data: Data, type: T.Type, complitionHendler: (_ responseModel: T) -> Void) {
        do {
            let responseModel = try JSONDecoder().decode(type.self, from: data)
                complitionHendler(responseModel)
            observer.onCompleted()
        } catch let error {
            observer.onError(GeneralError.simpleError(error))
            observer.onCompleted()
        }
    }
}
