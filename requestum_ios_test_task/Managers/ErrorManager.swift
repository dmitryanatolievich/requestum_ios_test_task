//
//  ErrorManager.swift
//  requestum_ios_test_task
//
//  Created by Dmitry Anatolievich on 01.04.2021.
//

import Foundation
import RxSwift
import RxCocoa

struct ErrorManager {
    
    static func errorHandler(_ errorType: ErrorType) -> String {
        switch errorType {
        case .model(let errorModel):
            if let message = errorModel.first?.message {
                return message
            } else {
                return "Server error"
            }
        }
    }
}

enum GeneralError: Error {
    
    case internalServer // 500
    case simpleError(Error)
    case errorType(ErrorType)
    case unknown
    
    var msg: String {
        switch self {
        case .internalServer:
            return "Internal Server Error. Please try again later"
        case .simpleError(let error):
            return error.localizedDescription
        case .errorType(let errorType):
            return ErrorManager.errorHandler(errorType)
        case .unknown:
            return "Unknown Error"
        }
    }
}

struct ErrorModel: Decodable {
    var code: Int?
    var message: String?
    var field: String?
    var userFacingMessage: String?
}

enum ErrorType: Decodable {
    
    case model(errorModel: [ErrorModel])
    
    init(from decoder: Decoder) throws {
        if let model = try? decoder.singleValueContainer().decode([ErrorModel].self) {
            self = .model(errorModel: model)
            return
        }
        throw GeneralError.unknown
    }
}
