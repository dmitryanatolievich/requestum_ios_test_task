//
//  SafariManager.swift
//  requestum_ios_test_task
//
//  Created by Dmitry Anatolievich on 01.04.2021.
//

import Foundation
import SafariServices

class SafariManager: NSObject {
    
    func initWebsiteVC(by url: String) {
        if let websiteUrl = URL(string: url) {
            let safariVC = SFSafariViewController(url: websiteUrl)
            safariVC.delegate = self
            self.presentVC(safariVC)
        }
    }
}

extension SafariManager: SFSafariViewControllerDelegate {
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
