//
//  MainTableViewCell.swift
//  requestum_ios_test_task
//
//  Created by Dmitry Anatolievich on 01.04.2021.
//

import UIKit

class MainTableViewCell: UITableViewCell {
    @IBOutlet weak var mainLbl: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var websiteLbl: UILabel!
    @IBOutlet weak var webLbl: UILabel!
    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var streetNameLbl: UILabel!
    @IBOutlet weak var stateLbl: UILabel!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var streetLbl: UILabel!
    @IBOutlet weak var showOnMapBtn: UIButton!
    
    var controller = MainController()
    var indexPathRow = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureCell()
    }
    
    var model: BreweriesModel? {
        didSet {
            guard let model = model else { return }
            setupContent(for: model)
        }
    }
    
    // MARK: Setup content for model
    
    private func setupContent(for model: BreweriesModel) {
        if model.street == nil {
            streetNameLbl.isHidden = true
            streetLbl.isHidden = true
        }
        if model.phone == nil {
            phone.isHidden = true
            phoneLbl.isHidden = true
        }
        if model.url == nil {
            webLbl.isHidden = true
            websiteLbl.isHidden = true
        }
        mainLbl.text = model.name
        phoneLbl.text = model.phone
        webLbl.text = model.url
        countryLbl.text = model.country
        stateLbl.text = model.state
        cityLbl.text = model.city
        streetLbl.text = model.street
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    // MARK: Configure cell
    
    private func configureCell() {
        layer.frame.size.height = 0
        layer.backgroundColor = UIColor.systemGray6.cgColor
        layer.cornerRadius = 13
        layer.borderWidth = 1
        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.borderColor = R.color.mainGreenColor()?.cgColor
        configureCellElements()
    }
    
    // MARK: Configure cell elements
    
    private func configureCellElements() {
        showOnMapBtn.cornerRadius = 6
    }
    
    // MARK: Actions
    
    @IBAction func showOnMap(_ sender: Any) {
        controller.didTouchShowOnMap(indexPathRow)
    }
}
