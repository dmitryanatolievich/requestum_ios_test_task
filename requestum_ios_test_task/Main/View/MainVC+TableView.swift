//
//  MainVC+TableView.swift
//  requestum_ios_test_task
//
//  Created by Dmitry Anatolievich on 01.04.2021.
//

import Foundation
import UIKit

// MARK: UITableViewDataSource

extension MainVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return controller.filteredData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: MainTableViewCell.identifier, for: indexPath) as? MainTableViewCell {
            cell.model = controller.filteredData?.get(index: indexPath.section)
            cell.indexPathRow = indexPath.row
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let selectedBrewery = controller.filteredData?[indexPath.section] else { return }
        controller.saveItemModel(selectedBrewery)
        controller.selectedBrewery = selectedBrewery
        controller.didTouchCell()
        if (selectedBrewery.latitude == nil) && (selectedBrewery.longitude == nil) {
            showAlertWithImage(title: "ERROR!!!", message: "Location not found!", image: R.image.error())
        } // Show alert if coordinates not found
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView()
        footerView.backgroundColor = .clear
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
}

// MARK: UITableViewDelegate

extension MainVC: UITableViewDelegate {}
