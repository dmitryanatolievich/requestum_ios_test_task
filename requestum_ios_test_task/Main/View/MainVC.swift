//
//  MainVC.swift
//  requestum_ios_test_task
//
//  Created by Dmitry Anatolievich on 01.04.2021.
//

import UIKit

class MainVC: UIViewController {
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var breweriesTableView: UITableView!
    @IBOutlet weak var spinnerView: UIActivityIndicatorView!
    
    var controller = MainController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureInterface()
        controller.initItems()
        controller.updateTableView = {
            self.breweriesTableView.reloadData()
        }
        controller.saveReceivedBreweriesToDB()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        controller.getBreweriesFromDB()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        searchBar.resignFirstResponder()
    }
    
    // MARK: Configure Interface
    
    private func configureInterface() {
        configureSpinner()
        configureTableView()
        configureSearchBar()
    }
    
    // MARK: Configure Table View
    
    private func configureTableView() {
        breweriesTableView.dataSource = self
        breweriesTableView.delegate = self
        breweriesTableView.register(cell: MainTableViewCell.self)
        breweriesTableView.backgroundColor = .clear
    }
    
    // MARK: Configure Search Bar
    
    private func configureSearchBar() {
        searchBar.delegate = self
        searchBar.searchTextField.backgroundColor = .white
        searchBar.placeholder = "Search"
        searchBar.searchTextField.textColor = .systemGray6
        var offset = UIOffset()
        let placeholderWidth = 110
        offset = UIOffset(horizontal: (searchBar.frame.width - CGFloat(placeholderWidth)) / 2, vertical: 0)
        searchBar.setPositionAdjustment(offset, for: .search)
    }
    
    // MARK: Configure Spinner
    
    private func configureSpinner() {
        controller.startSpinner = { [weak self] in
            guard let self = self else { return }
            self.spinnerView.startAnimating()
        }
        controller.stopSpinner = { [weak self] in
            guard let self = self else { return }
            self.spinnerView.stopAnimating()
        }
    }
}
