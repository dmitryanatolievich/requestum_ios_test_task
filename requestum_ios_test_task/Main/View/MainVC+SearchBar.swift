//
//  MainVC+SearchBar.swift
//  requestum_ios_test_task
//
//  Created by Dmitry Anatolievich on 04.04.2021.
//

import Foundation
import UIKit

extension MainVC: UISearchBarDelegate {
    
    // MARK: Search
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        controller.filteredData = []
        if searchText == "" {
            searchBar.perform(#selector(self.resignFirstResponder), with: nil, afterDelay: 0.3)
            controller.filteredData = controller.viewedBreweries
        } else {
            controller.viewedBreweries.forEach({ brewery in
                guard let name = brewery.name?.lowercased() else { return }
                if name.contains(searchText.lowercased()) {
                    controller.filteredData?.append(brewery)
                }
            })
        }
        breweriesTableView.reloadData()
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
