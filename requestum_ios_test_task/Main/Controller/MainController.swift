//
//  MainController.swift
//  requestum_ios_test_task
//
//  Created by Dmitry Anatolievich on 01.04.2021.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

protocol MainControllerProtocol {
    func initVC()
}

class MainController: ResponseManager {
    
    var viewedBreweries = [BreweriesModel]()
    var filteredData: [BreweriesModel]?
    var selectedBrewery = BreweriesModel()
    var getItems: (([BreweriesModel]) -> Void)?
    var updateTableView: (() -> Void)?
    var startSpinner: (() -> Void)?
    var stopSpinner: (() -> Void)?
    
    // rx
    var breweries = BehaviorRelay<[BreweriesModel]?>(value: nil)
    var breweriesFromDB = BehaviorRelay<[BreweriesModel]?>(value: nil)
    let disposeBag = DisposeBag()
    
    lazy var didTouchCell = {
        let breweriesDetailsController = BreweriesDetailsController()
        breweriesDetailsController.selectedBrewery = self.selectedBrewery
        breweriesDetailsController.initVC()
    }
    
    lazy var didTouchShowOnMap: (Int) -> Void = { [weak self] index in
        let showOnMapController = ShowOnMapController()
        let longitude = self?.filteredData?[index].longitude
        let latitude = self?.filteredData?[index].latitude
        showOnMapController.breweryLatitude = latitude ?? ""
        showOnMapController.breweryLongitude = longitude ?? ""
        showOnMapController.initVC()
    }
    
    func initItems() {
        startSpinner?()
        getBreweriesFromDB()
        requestBreweries()
        setupBinding()
    }
    
    // MARK: Setup binding
    
    func setupBinding() {
        breweries
            .asObservable()
            .bind { [weak self] breweries in
                guard let self = self, let breweries = breweries else { return }
                self.viewedBreweries = breweries
                self.filteredData = breweries
                self.viewedBreweries.sort(by: { $0.name ?? "" > $1.name ?? "" })
                self.updateTableView?()
            }.disposed(by: disposeBag)
    }
    
    // MARK: Request
    
    func requestBreweries() {
        getBreweries()
            .take(until: rx.deallocated)
            .subscribe(on: ConcurrentMainScheduler.instance)
            .subscribe(onNext: { [weak self] breweries in
                self?.stopSpinner?()
                guard let self = self else { return }
//                self.breweries.accept(breweries?.filter({ $0.longitude != nil }))
                self.breweries.accept(breweries)
            }, onError: { error in
                self.stopSpinner?()
                print("‼️ Error: \(error.localizedDescription)")
            }).disposed(by: disposeBag)
    }
    
    private func getBreweries() -> Observable<[BreweriesModel]?> {
        return Observable<[BreweriesModel]?>.create({ (observer) -> Disposable in
            return API.BreweryItem.getBreweries.responseSimpleData()
                .subscribe(onNext: { data in
                    self.parsingData(observer: observer, data: data, type: [BreweriesModel].self, complitionHendler: { responseModel in
                        let breweries = responseModel
                        observer.onNext(breweries)
                    })
                }, onError: { error in
                    observer.onError(GeneralError.simpleError(error))
                    observer.onCompleted()
                })
        })
    }
    
    // MARK: Get saved breweries from Realm
    
    func getBreweriesFromDB() {
        var breweriesFromRealm = try? RealmManager.shared.readObjects(type: BreweriesModelDB.self)
        print("✅🗂 breweriesDB COUNT from RealmDB = \(breweriesFromRealm?.count ?? 0)")
        breweriesFromRealm?.sort(by: { $0.date > $1.date })
        if let models = breweriesFromRealm?.map({BreweriesModel(breweriesModel: $0)}) {
            breweriesFromDB.accept(models)
        }
    }
    
    // MARK: Save breweries to Realm
    
    func saveItemModel(_ itemModel: BreweriesModel) {
        if var breweriesFromRealm = try? RealmManager.shared.readObjects(type: BreweriesModelDB.self) {
            breweriesFromRealm.sort(by: { $0.date > $1.date })
//            if breweriesFromRealm.count > 9 {
//                let filtered = Array(breweriesFromRealm.prefix(9))
//                RealmManager.shared.deleteObjects(besides: filtered)
//            }
        }
        RealmManager.shared.writeObject(BreweriesModelDB(breweriesModel: itemModel))
    }
    
    func saveReceivedBreweriesToDB() {
        guard let breweriesFromRealm = try? RealmManager.shared.readObjects(type: BreweriesModelDB.self) else { return }
        RealmManager.shared.writeObjects(breweriesFromRealm)
    }
}
