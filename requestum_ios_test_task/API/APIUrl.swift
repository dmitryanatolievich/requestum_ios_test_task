//
//  APIUrl.swift
//  requestum_ios_test_task
//
//  Created by Dmitry Anatolievich on 04.04.2021.
//

import Foundation

enum Server: String {
    case url = "https://api.openbrewerydb.org"
}

let serverUrl = "\(Server.url.rawValue)/breweries"
