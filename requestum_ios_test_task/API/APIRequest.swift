//
//  APIRequest.swift
//  requestum_ios_test_task
//
//  Created by Dmitry Anatolievich on 04.04.2021.
//

import Foundation
import Alamofire

struct APIRequest {
    let url: String
    let httpMethod: HTTPMethod
    let body: [String: AnyObject]?
    let header: HTTPHeaders?
    
    static func createRequest(url: String, httpMethod: HTTPMethod, body: [String: AnyObject]? = nil) -> APIRequest {
        return self.init(url: url, httpMethod: httpMethod, body: body, header: HTTPHeaders())
    }
}
