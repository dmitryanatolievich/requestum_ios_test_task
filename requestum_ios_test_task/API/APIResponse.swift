//
//  APIResponse.swift
//  requestum_ios_test_task
//
//  Created by Dmitry Anatolievich on 04.04.2021.
//

import Foundation
import Alamofire
import RxSwift

protocol APIResponse {
    var request: APIRequest { get }
}

extension APIResponse {
    
    // MARK: - REQUEST/RESPONSE
    
    func responseSimpleData() -> Observable<Data> {
        showRequestLogs()
        return Observable.create { observer -> Disposable in
            AF.request(self.request.url, method: self.request.httpMethod, parameters: self.request.body, encoding: JSONEncoding.default, headers: self.request.header)
                .validate()
                .responseData(completionHandler: { (response) in
                    if let statusCode = response.response?.statusCode {
                        response.error?.printError(with: statusCode)
                        if statusCode == 500 {
                            observer.onError(GeneralError.internalServer)
                            observer.onCompleted()
                        }
                    }
                    if let data = response.data {
                        self.showDataLogs(with: data)
                        observer.onNext(data)
                    } else {
                        self.showFailureLogs(with: response.error ?? GeneralError.unknown)
                        observer.onError(response.error ?? GeneralError.unknown)
                        return
                    }
            })
            return Disposables.create()
        }
    }
    
    // MARK: - LOGS
    
    private func showRequestLogs() {
        print("""
            ✴️⬇️
            URL: \(request.url);
            HTTP METHOD: \(request.httpMethod);
            BODY: \(request.body ?? [:]);
            HEADER: \(request.header ?? [:]);
            ⬆️✴️
            """)
    }
    
    private func showDataLogs(with data: Data) {
        do {
            let decoded = try JSONSerialization.jsonObject(with: data, options: [])
            if let dictFromJSON = decoded as? [String: AnyObject] {
                print("""
                    ✅⬇️ Data Response (url = \(self.request.url)):
                    \(dictFromJSON)
                    ⬆️✅
                    """)
            }
        } catch {
            showFailureLogs(with: error)
        }
    }
    
    private func showFailureLogs(with error: Error) {
        print("""
            ❌⬇️ Failure Response (url = \(self.request.url)):
            \(error.localizedDescription)"
            ⬆️❌
            """)
    }
}
