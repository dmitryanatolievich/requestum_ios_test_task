//
//  Reachability.swift
//  requestum_ios_test_task
//
//  Created by Dmitry Anatolievich on 14.04.2021.
//

import Reachability

// Reachability
// declare this property where it won't go out of scope relative to your listener

private var reachability: Reachability!

protocol ReachabilityActionDelegate {
    func reachabilityChanged(_ isReachable: Bool)
}

protocol ReachabilityObserverDelegate: class, ReachabilityActionDelegate {
    func addReachabilityObserver() throws
    func removeReachabilityObserver()
}

// Declaring default implementation of adding/removing observer

extension ReachabilityObserverDelegate {
    
    /** Subscribe on reachability changing */
    func addReachabilityObserver() throws {
        reachability = try Reachability()
        
        reachability.whenReachable = { [weak self] _ in
            self?.reachabilityChanged(true)
        }
        
        reachability.whenUnreachable = { [weak self] _ in
            self?.reachabilityChanged(false)
        }
        
        try reachability.startNotifier()
    }
    
    /** Unsubscribe */
    func removeReachabilityObserver() {
        reachability.stopNotifier()
        reachability = nil
    }
}

class ReachabilityHandler: ReachabilityObserverDelegate {
  
  // MARK: Lifecycle
    
    var controller: UIViewController? {
        return UIApplication.shared.keyWindow?.rootViewController
    }
  
  required init() {
    try? addReachabilityObserver()
  }
  
  deinit {
    removeReachabilityObserver()
  }
  
  // MARK: Reachability
    
  func reachabilityChanged(_ isReachable: Bool) {
    if !isReachable {
        controller?.fireNoInternetController(false)
    } else {
        controller?.fireNoInternetController(true)
    }
}

}
