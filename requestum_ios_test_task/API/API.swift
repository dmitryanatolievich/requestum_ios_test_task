//
//  API.swift
//  requestum_ios_test_task
//
//  Created by Dmitry Anatolievich on 04.04.2021.
//

import Foundation

// MARK: - API

class API {
    enum BreweryItem {
        case getBreweries
    }
}

// MARK: - BreweryItem

extension API.BreweryItem: APIResponse {
    
    var path: String {
        switch self {
        case .getBreweries:
            return "\(serverUrl)"
        default:
            break
        }
    }
    
    var request: APIRequest {
        switch self {
        case .getBreweries:
            return APIRequest.createRequest(
                url: path,
                httpMethod: .get)
        }
    }
}
