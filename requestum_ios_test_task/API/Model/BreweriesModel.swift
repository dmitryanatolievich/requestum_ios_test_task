//
//  BreweriesModel.swift
//  requestum_ios_test_task
//
//  Created by Dmitry Anatolievich on 04.04.2021.
//

import Foundation

class BreweriesResponseModel: ResponseProtocol {
    var data: [BreweriesModel]?
    var errors: ErrorType?
}

class BreweriesModel: ResponseProtocol {
    var errors: ErrorType?
    
    var id: Int?
    var name: String?
    var street: String?
    var city: String?
    var state: String?
    var country: String?
    var longitude: String?
    var latitude: String?
    var phone: String?
    var url: String?
    
    convenience init(breweriesModel: BreweriesModelDB) {
        self.init()
        self.id = breweriesModel.id
        self.name = breweriesModel.name
        self.street = breweriesModel.state
        self.city = breweriesModel.city
        self.state = breweriesModel.state
        self.country = breweriesModel.country
        self.longitude = breweriesModel.longitude
        self.latitude = breweriesModel.latitude
        self.phone = breweriesModel.phone
        self.url = breweriesModel.url
    }
}
