//
//  ShowOnMapController.swift
//  requestum_ios_test_task
//
//  Created by Dmitry Anatolievich on 14.04.2021.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import MapKit

class ShowOnMapController: NSObject, MainControllerProtocol {
    
    var selectedBrewery = BreweriesModel()
    var breweryLatitude: String = ""
    var breweryLongitude: String = ""
    
    lazy var didTouchBackBtn = {
        self.popVC()
    }
    
    // MARK: Init VC
    
    func initVC() {
        guard let showOnMapVC = R.storyboard.showOnMap.showOnMapVC() else { return }
        showOnMapVC.controller = self
        pushVC(showOnMapVC)
    }
}
