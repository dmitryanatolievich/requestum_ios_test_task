//
//  ShowOnMapVC+Location.swift
//  requestum_ios_test_task
//
//  Created by Dmitry Anatolievich on 14.04.2021.
//

import MapKit

extension ShowOnMapVC: CLLocationManagerDelegate {
   
    // MARK: Configure show on map

    func showLocation() {
        annotation.coordinate = CLLocationCoordinate2D(latitude: breweryLatitude ?? 0, longitude: breweryLongitude ?? 0)
        mapView.addAnnotation(annotation)
        let breweryLocation = CLLocationCoordinate2D(latitude: breweryLatitude ?? 0, longitude: breweryLongitude ?? 0)
        mapView.setCenter(breweryLocation, animated: true)
    }
}
