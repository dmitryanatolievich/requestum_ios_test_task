//
//  ShowOnMapVC.swift
//  requestum_ios_test_task
//
//  Created by Dmitry Anatolievich on 14.04.2021.
//

import UIKit
import RxSwift
import RxCocoa
import MapKit

class ShowOnMapVC: UIViewController {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var mapView: MKMapView!

    let annotation = MKPointAnnotation()
    var controller = ShowOnMapController()
    var breweryLatitude: Double? = 0
    var breweryLongitude: Double? = 0
        
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLbl.text = controller.selectedBrewery.name
        showLocation()
        breweryLatitude = convertStringToDouble(string: controller.breweryLatitude)
        breweryLongitude = convertStringToDouble(string: controller.breweryLongitude)
        checkCoordinate(latitude: breweryLatitude, longitude: breweryLongitude, mapView: mapView)
        if (breweryLatitude == 0.0) && (breweryLongitude == 0.0) {
            showAlertWithImage(title: "ERROR!!!", message: "Location not found!", image: R.image.error())
        } // Show alert if coordinates not found
        print(breweryLatitude!)
        print(breweryLongitude!)
    }
    
    // MARK: Actions

    @IBAction func touchBackBtn(_ sender: Any) {
        controller.didTouchBackBtn()
    }
}
