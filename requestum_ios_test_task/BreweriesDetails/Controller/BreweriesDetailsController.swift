//
//  BreweriesDetailsController.swift
//  requestum_ios_test_task
//
//  Created by Dmitry Anatolievich on 01.04.2021.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

class BreweriesDetailsController: NSObject, MainControllerProtocol {
    
    var titleText = ""
    var selectedBrewery = BreweriesModel()
    
    lazy var didTouchBackBtn = {
        self.popVC()
    }
    
    // MARK: Init VC

    func initVC() {
        guard let breweriesDetailsVC = R.storyboard.breweriesDetails.breweriesDetailsVC() else { return }
        breweriesDetailsVC.controller = self
        pushVC(breweriesDetailsVC)
    }
}
