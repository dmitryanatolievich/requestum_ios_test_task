//
//  BreweriesDetailsVC+Location.swift
//  requestum_ios_test_task
//
//  Created by Dmitry Anatolievich on 15.04.2021.
//

import MapKit

extension BreweriesDetailsVC: CLLocationManagerDelegate {

    // MARK: Configure show on map
    
    func showLocation() {
        annotation.coordinate = CLLocationCoordinate2D(latitude: breweryLatitude, longitude: breweryLongitude)
        
        mapView.addAnnotation(annotation)
        let breweryLocation = CLLocationCoordinate2D(latitude: breweryLatitude, longitude: breweryLongitude)
        mapView.setCenter(breweryLocation, animated: true)
    }
}
