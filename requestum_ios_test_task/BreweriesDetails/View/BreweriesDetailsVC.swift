//
//  BreweriesDetailsVC.swift
//  requestum_ios_test_task
//
//  Created by Dmitry Anatolievich on 01.04.2021.
//

import UIKit
import RxSwift
import RxCocoa
import MapKit

class BreweriesDetailsVC: UIViewController {
    
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var webLbl: UILabel!
    @IBOutlet weak var streetNameLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var websiteLbl: UILabel!
    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var stateLbl: UILabel!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var streetLbl: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    var annotation = MKPointAnnotation()
    
    var controller = BreweriesDetailsController()
    var breweryLatitude: Double = 0.0
    var breweryLongitude: Double = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backBtn.tintColor = .white
        mapView.isHidden = false
        setupContent(for: controller.selectedBrewery)
        checkCoordinate(latitude: breweryLatitude, longitude: breweryLongitude, mapView: mapView)
        showLocation()
    }
    
    // MARK: Setup content for model
    
    private func setupContent(for selectedBrewery: BreweriesModel) {
        if selectedBrewery.street == nil {
            streetNameLbl.isHidden = true
            streetLbl.isHidden = true
        }
        if selectedBrewery.phone == nil {
            phone.isHidden = true
            phoneLbl.isHidden = true
        }
        if selectedBrewery.url == nil {
            webLbl.isHidden = true
            websiteLbl.isHidden = true
        }
        titleLbl.text = selectedBrewery.name
        phoneLbl.text = selectedBrewery.phone
        websiteLbl.text = selectedBrewery.url
        countryLbl.text = selectedBrewery.country
        stateLbl.text = selectedBrewery.state
        cityLbl.text = selectedBrewery.city
        streetLbl.text = selectedBrewery.street
        breweryLatitude = convertStringToDouble(string: selectedBrewery.latitude ?? "")
        breweryLongitude = convertStringToDouble(string: selectedBrewery.longitude ?? "")
        print(breweryLatitude)
        print(breweryLongitude)
    }
    
    // MARK: Actions
    
    @IBAction func touchBackBtn(_ sender: Any) {
        controller.didTouchBackBtn()
    }
}
