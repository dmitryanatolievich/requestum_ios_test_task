//
//  UIViewController.swift
//  requestum_ios_test_task
//
//  Created by Dmitry Anatolievich on 14.04.2021.
//

import UIKit

extension UIViewController {
    
    func fireNoInternetController(_ isReachable: Bool) {
        if isReachable == true {
            print(isReachable)
        } else {
            print(isReachable)
        }
    }
    
    func convertStringToDouble(string: String) -> Double {
        let numberFormatter = NumberFormatter()
        let number = numberFormatter.number(from: string )
        let doubleValue = number?.doubleValue ?? 0
        return doubleValue
    }
    
    func showAlertWithImage(title: String, message: String, image: UIImage?) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            if let image = image, let view = alert.view {
                let imageView = UIImageView(frame: CGRect(x: Int(250/2 - 40), y: 70, width: 100, height: 100))
                imageView.image = image
                alert.view.addSubview(imageView)
                let height = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 230)
                let width = NSLayoutConstraint(item: view, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 250)
                alert.view.addConstraint(height)
                alert.view.addConstraint(width)
            }
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in }))
            if var topController = UIApplication.shared.windows.first?.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }
                topController.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func checkCoordinate(latitude: Double?, longitude: Double?, mapView: UIView) {
        if latitude == 0.0 || longitude == 0.0 {
            mapView.isHidden = true
        }
    }
}
