//
//  Array.swift
//  requestum_ios_test_task
//
//  Created by Dmitry Anatolievich on 01.04.2021.
//

import Foundation

extension Array {
    
    // Safely lookup an index that might be out of bounds,
    // returning nil if it does not exist
    func get(index: Int) -> Element? {
        if 0 <= index && index < count {
            return self[index]
        } else {
            return nil
        }
    }
}
