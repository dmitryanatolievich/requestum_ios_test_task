//
//  NSObject.swift
//  requestum_ios_test_task
//
//  Created by Dmitry Anatolievich on 01.04.2021.
//

import Foundation
import UIKit

extension NSObject {
    
    func pushVC(_ vc: UIViewController) {
        if let rootNavVC = UIApplication.shared.windows.first?.rootViewController as? UINavigationController {
            rootNavVC.pushViewController(vc, animated: true)
        }
    }
    
    func popVC() {
        if let rootNavVC = UIApplication.shared.windows.first?.rootViewController as? UINavigationController {
            rootNavVC.popViewController(animated: true)
        }
    }
    
    func presentVC(_ vc: UIViewController) {
        if let rootNavVC = UIApplication.shared.windows.first?.rootViewController as? UINavigationController {
            rootNavVC.present(vc, animated: true, completion: nil)
        }
    }
    
    func showSuccess(message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Success", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in }))
            if var topController = UIApplication.shared.windows.first?.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }
                topController.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func showError(message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in }))
            if var topController = UIApplication.shared.windows.first?.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }
                topController.present(alert, animated: true, completion: nil)
            }
        }
    }
}
