//
//  Error.swift
//  requestum_ios_test_task
//
//  Created by Dmitry Anatolievich on 01.04.2021.
//

import Foundation

extension Error {
    
    func printError(with code: Int) {
        print("❌ Error code: \(code):")
        switch code {
        case 300...308:
            print("Redirection Error ❌")
        case 400:
            print("Bad Request Error ❌")
        case 401:
            print("Unauthorized Error ❌")
        case 402:
            print("Payment Required Error ❌")
        case 403:
            print("Not enough permissions to perform this action Error ❌")
        case 404:
            print("Client not found Error ❌")
        case 405...420:
            print("Some Client Error ❌")
        case 422:
            print("Error: \(self.localizedDescription) ❌")
        case 423...499:
            print("Some Client Error ❌")
        case 500:
            print("500 Error ❌")
        case 501...599:
            print("Some Server Error ❌")
        default:
            print("Unknown Error ❌")
        }
    }
}
