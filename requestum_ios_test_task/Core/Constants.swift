//
//  Constants.swift
//  requestum_ios_test_task
//
//  Created by Dmitry Anatolievich on 01.04.2021.
//

import Foundation

// ---Realm
enum ErrorResultRealm: Error {
    case notFoundElements
    case notExistModel
}
