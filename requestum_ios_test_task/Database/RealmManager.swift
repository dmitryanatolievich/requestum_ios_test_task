//
//  RealmManager.swift
//  requestum_ios_test_task
//
//  Created by Dmitry Anatolievich on 01.04.2021.
//

import Foundation
import RealmSwift

class RealmManager {
    
    // MARK: Singleton
    
    static let shared = RealmManager()
    
    var realm = try? Realm()
    
    func writeObject(_ object: Object) {
        let realm = try? Realm()
        try? realm?.write {
            realm?.add(object, update: .all)
        }
    }
    
    func writeObjects(_ object: [Object]) {
        let realm = try? Realm()
        try? realm?.write {
            realm?.add(object, update: .all)
        }
    }
    
    func deleteObjects(besides items: [BreweriesModelDB]) {
        let realm = try? Realm()
        let itemsId = items.map { $0.id }
        if let itemsForDelete = realm?.objects(BreweriesModelDB.self).filter("NOT id IN %@", itemsId) {
            try? realm?.write {
                realm?.delete(itemsForDelete)
            }
        }
    }

    func readObjects<T>(type: T.Type) throws -> [T] where T: Object {
        guard let object = try? realm?.objects(type) else {
            throw ErrorResultRealm.notExistModel
        }
        guard !object.isEmpty else {
            throw ErrorResultRealm.notFoundElements
        }
        return Array(object)
    }

}
