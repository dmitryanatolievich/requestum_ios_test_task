//
//  BreweriesModelDB.swift
//  requestum_ios_test_task
//
//  Created by Dmitry Anatolievich on 01.04.2021.
//

import Foundation
import Realm
import RealmSwift

class BreweriesModelDB: Object {
    
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var street: String = ""
    @objc dynamic var city: String = ""
    @objc dynamic var state: String = ""
    @objc dynamic var country: String = ""
    @objc dynamic var longitude: String = ""
    @objc dynamic var latitude: String = ""
    @objc dynamic var phone: String = ""
    @objc dynamic var url: String = ""
    @objc dynamic var date = Date()

    convenience init(breweriesModel: BreweriesModel) {
        self.init()
        self.id = breweriesModel.id ?? 0
        self.name = breweriesModel.name ?? ""
        self.street = breweriesModel.street ?? ""
        self.city = breweriesModel.city ?? ""
        self.state = breweriesModel.state ?? ""
        self.country = breweriesModel.country ?? ""
        self.longitude = breweriesModel.longitude ?? ""
        self.latitude = breweriesModel.latitude ?? ""
        self.phone = breweriesModel.phone ?? ""
        self.url = breweriesModel.url ?? ""
        self.date = Date()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
